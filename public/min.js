const shipPadding = 5

let shipVelocity = 0

const startTime = Date.now()

const app = new PIXI.Application(800, 600, { antialias: true })
const { stage, screen } = app

const graphics = new PIXI.Graphics()
stage.addChild(graphics)

const ship = new PIXI.Rectangle(0, 0, 100, 40)
ship.x = (screen.width - ship.width) / 2
ship.y = screen.height - ship.height - shipPadding

const run = delta => {
  const remainingSeconds = 198 - Math.round((Date.now() - startTime) / 1000)

  const title = document.querySelector('h1')
  title.innerText = `${remainingSeconds} seconds in space`

  graphics.clear()

// set a fill and line style
  graphics.beginFill(0xFF3300)
  graphics.lineStyle(4, 0xffd900, 1)

// draw a shape
  graphics.moveTo(50, 50)
  graphics.lineTo(250, 50)
  graphics.lineTo(100, 100)
  graphics.lineTo(50, 50)
  graphics.endFill()

// set a fill and a line style again and draw a rectangle
  graphics.lineStyle(2, 0x0000FF, 1)
  graphics.beginFill(0xFF700B, 1)
  graphics.drawRect(50, 250, 120, 120)

// draw a rounded rectangle
  graphics.lineStyle(2, 0xFF00FF, 1)
  graphics.beginFill(0xFF00BB, 0.25)
  graphics.drawRoundedRect(ship.x, ship.y, ship.width, ship.height, 15)
  graphics.endFill()

// draw a circle, set the lineStyle to zero so the circle doesn't have an outline
  graphics.lineStyle(0)
  graphics.beginFill(0xFFFF0B, 0.5)
  graphics.drawCircle(470, 90, 60)
  graphics.endFill()

  ship.x = Math.max(
    shipPadding,
    Math.min(
      screen.width - ship.width - shipPadding,
      ship.x + shipVelocity * delta
    )
  )
}

const onKeyDown = event => {
  if (shipVelocity !== 0) {
    return
  }

  const { key } = event
  switch (key) {
    case 'ArrowLeft':
      shipVelocity = -5
      break
    case 'ArrowRight':
      shipVelocity = 5
      break
  }
}

const onKeyUp = event => {
  const { key } = event
  switch (key) {
    case 'ArrowLeft':
    case 'ArrowRight':
      shipVelocity = 0
      break
  }

}

document.body.appendChild(app.view)
app.ticker.add(run)

document.addEventListener('keydown', onKeyDown)
document.addEventListener('keyup', onKeyUp)

